var shown = LOAD_MORE_ROWS;
var loadFeeds = function () {
    var loadMoreBtn = false;
    var request = new XMLHttpRequest();

    request.open('GET', API_END_POINT + '/feed.json', true);
    request.onload = function () {

        // Begin accessing JSON data here
        var data = JSON.parse(this.response);

        if (request.status >= 200 && request.status < 400) {
            var items = data.data.rows;

            var grid = "";
            var rowNum = 1;
            var rowToggle = true;
            var gridNum = 0;
            var rowDivStatus = "closed";

            items.forEach(item => {

                if (!(gridNum % 3)) {
                    grid = grid + "<div class='grid-container gridcontainer" + rowToggle + "' id='rownumber" + rowNum + "'";
                    if ((rowNum > LOAD_MORE_ROWS) && (!loadAll)) {
                        grid = grid + " style='display:none' ";
                    }
                    grid = grid + ">";

                    rowDivStatus = "open";
                }

                grid = grid + "<div class='grid-item'><h3>" + item.title + "</h3></div>";
//<img src='" + item.thumbnail + "' />
                if ((gridNum % 3) == 2) {
                    grid = grid + "</div>";
                    rowToggle = !rowToggle;
                    rowDivStatus = "closed";
                    rowNum++;
                }
                gridNum++;
            });
            if (rowDivStatus == "open") {
                grid = grid + "</div>";
            }
            if ((rowNum > LOAD_MORE_ROWS) && (!loadAll)) {
                grid = grid + "<div id='loadmore'><button id='loadmorebtn'>Load more</button></div>";
                loadMoreBtn = true;
            }
            document.getElementById("allFeeds").innerHTML = grid;
            if (loadMoreBtn) {
                //Set event Listener
                document.getElementById("loadmorebtn").addEventListener('click', toggle);
            }
        } else {
            console.log('error');
        }
    }

    request.send();
}

var labelChange = function () {
    document.getElementById("loadmore").innerHTML = "No more items";
}

var toggle = function () {
    for (var i = 0; i < LOAD_MORE_ROWS; i++) {
        shown = shown + 1;
        elem = document.getElementById("rownumber" + shown);

        if (typeof (elem) !== 'undefined' && elem !== null)
        {
            elem.style.display = "grid";
        }
    }

    var nextElm = shown + 1;
    var nextElement = document.getElementById("rownumber" + nextElm);
    if (typeof (nextElement) === 'undefined' || nextElement === null)
    {
        document.getElementById("loadmore").innerHTML = "<label>No more items!</label>"
    }
}
// Run when Page is ready
window.onload = function () {
    loadFeeds();
}