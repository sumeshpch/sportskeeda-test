<?php

/**
 * Curlrequest class
 *
 * @author  sumesh <sumeshp@hifx.co.in>
 */
Class Curlrequest {

    private $_ch;
    // default config
    private $_config = array(
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_AUTOREFERER => true,
        CURLOPT_CONNECTTIMEOUT => 100,
        CURLOPT_TIMEOUT => 100,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:5.0) Gecko/20110619 Firefox/5.0'
    );

    // initialize curl
    public function __construct() {

        try {
            $this->_ch = curl_init();

            $this->setOptions($this->_config);
        } catch (Exception $e) {
            throw new CException('Curl not installed');
        }
    }

    //execute curl
    public function _exec() {
        $c = curl_exec($this->_ch);
        if (!curl_errno($this->_ch)) {
            curl_close($this->_ch);
            return $c;
        } else {
            throw new CException(curl_error($this->_ch));
            curl_close($this->_ch);
            return false;
        }
    }

    /** Sumesh */
    public function setProxy($proxy, $port) {
        $this->setOption(CURLOPT_PROXY, $proxy);
        $this->setOption(CURLOPT_PROXYPORT, $port);
        return $this;
    }

    //set a curl option
    public function setOption($option, $value) {
        curl_setopt($this->_ch, $option, $value);
        return $this;
    }

    //set curl options from array
    public function setOptions($options = array()) {
        curl_setopt_array($this->_ch, $options);
        return $this;
    }

    //set curl options from array
    public function setHeader($curlHeader) {
        curl_setopt($this->_ch, CURLOPT_HTTPHEADER, $curlHeader);
        return $this;
    }

}
