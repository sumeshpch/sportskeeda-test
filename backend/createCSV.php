<?php

require_once './settings.php';
require_once './Curlrequest.php';

/* read the endpoint using curl GET method */
$curlRequest = new Curlrequest;

$curl_config[CURLOPT_URL] = API_END_POINT;
$curlRequest->setOptions($curl_config);

$curlResponse = $curlRequest->_exec();

$feedResponse = json_decode($curlResponse, true);
//get the new feeds
$feeds = $feedResponse['cards'];

/* open the existing master file in read mode and save to an array($masterContent)
 * this process is done to write the new articles onto top of the master file
 * also create an array($masterIds) to hold the ID values from the existing master file
 */
$csv_master_filename = "./csv/sk_master.csv";
$masterExist = false;
$masterContent = $masterIds = array();
if (file_exists($csv_master_filename)) {
    $masterExist = true;

    //read master file content to an array
    $masterfile = fopen($csv_master_filename, "r");
    while ($row = fgetcsv($masterfile)) {
        array_push($masterContent, $row);
        array_push($masterIds, $row[0]);
    }
    fclose($masterfile);
}

/* open the master file in write mode */
$masterfd = fopen($csv_master_filename, "w");

//if the api endpoint responds with any articles
if (count($feeds)) {
    //create a new file to save the api response
    $csv_filename = "./csv/sk_csv_" . date("H:i_d_m_Y", time()) . ".csv"; //sk_csv_hour:minute_day_month_year
    $fd = fopen($csv_filename, "w");

    foreach ($feeds as $feed) {

        $categories = implode(",", $feed['category']);
        $fileContent = array($feed['ID'], $feed['title'], $feed['permalink'], $categories, $feed['read_count']);
        //write to the new file one by one
        fputcsv($fd, $fileContent);

        //check if the article ID already exists in master file and if not exist, then write to master file
        if ((!$masterExist) || ($masterExist && (!in_array($feed['ID'], $masterIds)))) {
            fputcsv($masterfd, $fileContent);
        }
    }
    //close the new file pointer
    fclose($fd);
}

//append the existing master file articles below the new article
if ($masterExist) {
    foreach ($masterContent as $existingMaster) {
        $fileContent = array($existingMaster[0], $existingMaster[1], $existingMaster[2], $existingMaster[3], $feed[4]);
        fputcsv($masterfd, $fileContent);
    }
}
//close master file pointer
fclose($masterfd);
